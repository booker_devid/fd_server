package model

//ActionVerificationData -
type ActionVerificationData struct {
	Type   string           `json:"event"`
	Object string           `json:"subject"`
	Data   VerificationData `json:"data"`
}

//VerificationData
type VerificationData struct {
	Verification bool   `json:"verification"`
	Comment      string `json:"comment"`
}

//ActionCreateOrderData
type ActionCreateOrderData struct {
	Type   string           `json:"event"`
	Object string           `json:"subject"`
	Data   OrderCreatedData `json:"data"`
}

//StartOrderData
type StartOrderData struct {
	Order    string `json:"order"`
	Driver   string `json:"driver"`
	Duration int    `json:"duration"`
}

//PositionData
type PositionData struct {
	Order string  `json:"order"`
	Lat   float64 `json:"lat"`
	Lon   float64 `json:"lon"`
}

//DataOrder
type DataOrder struct {
	Order string `json:"order"`
	Value string `json:"value"`
}

type ValueData struct {
	Value int `json:"value"`
}

//ActionCreateOrdersData
type ActionCreateOrdersData struct {
	Type   string             `json:"event"`
	Object string             `json:"subject"`
	Data   []OrderCreatedData `json:"data"`
}

//OrderCreatedData - Data order for driver
type OrderCreatedData struct {
	ID        string     `json:"id"`
	Name      string     `json:"username"`
	Phone     string     `json:"phone"`
	Img       string     `json:"img"`
	Rating    int        `json:"rating"`
	Status    string     `json:"status"`
	InCity    bool       `json:"in-city"` //True - in City/False - out City
	Places    []Place    `json:"places"`
	AddOption OptionData `json:"parametrs"`
}

//OfferDriverData - Data offer
type OfferDriverData struct {
	IDOrder  string  `json:"order"`
	Duration int     `json:"duration"`
	Lat      float64 `json:"lat"`
	Lon      float64 `json:"lon"`
}

//EventListOut - out_list event
type EventListOut struct {
	ID string `json:"id"`
}

type RecoveryAction struct {
	Type string               `json:"type"`
	Data OptionActionUpWorker `json:"data"`
}

type OptionActionUpWorker struct {
	Price   int    `json:"price"`
	Comment string `json:"comment"`
}

//ActionOfferDriverData - Data offer
// type ActionOfferDriverData struct {
// 	IDdOrder string  `json:"order"`
// 	IDUser   string  `json:"idOrder"`
// 	FullName string  `json:"name"`
// 	Image    string  `json:"img"`
// 	Price    int     `json:"price"`
// 	Duration int     `json:"duration"`

// 	Lat      float64 `json:"lat"`
// 	Lon      float64 `json:"lon"`
// }

// type DriverOrderData struct {
// 	IDDriver string           `json:"iddriver"`
// 	Name     string           `json:"username"`
// 	Phone    string           `json:"phone"`
// 	Duration string           `json:"duration"`
// 	Img      string           `json:"img"`
// 	Car      CarOrderData     `json:"car"`
// 	Partner  PartnerOrderData `json:"partner"`
// }

//EventTimeOutOrder - Data time out order
type EventTimeOutOrder struct {
	User  string `json:"user"`
	Order string `json:"order"`
}

//EventMessage
type EventMessage struct {
	User    string `json:"user"`
	Order   string `json:"order"`
	Owner   bool   `json:"owner"`
	Message string `json:"message"`
}

//WorkModelEventTimeOutOrder -
type WorkModelEvent struct {
	Type   string            `json:"event"`
	Object string            `json:"subject"`
	Data   EventTimeOutOrder `json:"data"`
}

//WorkModelEventTimeOutOrder -
type PositionEvent struct {
	Type   string         `json:"event"`
	Object string         `json:"subject"`
	Data   PositionAction `json:"data"`
}

//PositionAction
type PositionAction struct {
	User string  `json:"user"`
	Lat  float64 `json:"lat"`
	Lon  float64 `json:"lon"`
}

//WorkModelEventTimeOutOrder -
type ActionOnly struct {
	Type   string `json:"event"`
	Object string `json:"subject"`
}

//WorkModelDriverOrderDataOffer
type WorkModelDriverOrderDataOffer struct {
	Type   string               `json:"event"`
	Object string               `json:"subject"`
	Data   DriverOrderDataOffer `json:"data"`
}

//WorkModelOrderRecovery
type WorkModelOrderRecovery struct {
	Type   string               `json:"event"`
	Object string               `json:"subject"`
	Data   DriverOrderDataOffer `json:"data"`
}
