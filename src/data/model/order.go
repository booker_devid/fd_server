package model

//SendWebSocketDataClient - create
type SendWebSocketDataClient struct {
	Connect string            `json:"con"`
	Order   OrderModelForSend `json:"ord"`
}

//SocketClient - list data for user
type SocketClient struct {
	Token string              `json:"token"`
	Order OrdermodelforCreate `json:"order"`
}

//WorkModelRead - data for create order
type WorkModelRead struct {
	TypeAction string      `json:"type"`
	Value      interface{} `json:"data"`
}

//WorkModelSend - data for create order
type WorkModelSend struct {
	Type   string      `json:"event"`
	Object string      `json:"subject"`
	Data   interface{} `json:"data"`
}

//OrdermodelforCreate - model client order
type OrdermodelforCreate struct {
	Places    []Place     `json:"places"`
	AddOption OptionModel `json:"options"`
}

//OrderModelForSend - model for send to client
type OrderModelForSend struct {
	ID        string      `json:"id"`
	Status    string      `json:"status"`
	InCity    bool        `json:"type"` //True - in City/False - out City
	Places    []Place     `json:"places"`
	AddOption OptionModel `json:"option"`
}

//ReadyOrderModelClient - client data
type ReadyOrderModelClient struct {
	Actor     string          `json:"actor"`
	ID        string          `json:"sending"`
	IDOrder   string          `json:"order"`
	InCity    bool            `json:"in-city"` //True - in City/False - out City
	Places    []Place         `json:"places"`
	AddOption OptionData      `json:"parametrs"`
	Driver    DriverOrderData `json:"driver"`
}

//ReadyOrderModelDriver - driver data
type ReadyOrderModelDriver struct {
	Actor     string          `json:"actor"`
	ID        string          `json:"sending"`
	IDOrder   string          `json:"order"`
	InCity    bool            `json:"in-city"` //True - in City/False - out City
	Places    []Place         `json:"places"`
	AddOption OptionData      `json:"parametrs"`
	Client    ClientOrderData `json:"client"`
}

//OptionModel  - for OrderModelForSend
type OptionModel struct {
	Price     int    `json:"price"`
	Babych    bool   `json:"babych"`
	Smoking   bool   `json:"smoking"`
	FastUp    bool   `json:"fastup"`
	Comment   string `json:"comment"`
	Animal    bool   `json:"animal"`
	PlanOrder bool   `json:"plan_order"`
	PlanTime  string `json:"time_plan"`
	SystemPay bool   `json:"pay"`
}

//Place - Data Place for order
type Place struct {
	City      string  `json:"city"`
	Addr      string  `json:"addr"`
	TypePorch int     `json:"type_porch"`
	Porch     int     `json:"porch"`
	Lat       float64 `json:"lat"`
	Lon       float64 `json:"lon"`
}

//DriverOrderData - Data Driver for Order
type DriverOrderData struct {
	IDDriver string           `json:"iddriver"`
	Name     string           `json:"username"`
	Phone    string           `json:"phone"`
	Duration int              `json:"duration"`
	Rating   int              `json:"rating"`
	Img      string           `json:"img"`
	Car      CarOrderData     `json:"car"`
	Partner  PartnerOrderData `json:"partner"`
}

//ClientOrderData - Data Client for Order
type ClientOrderData struct {
	IDClient string `json:"iduser"`
	Name     string `json:"username"`
	Rating   int    `json:"rating"`
	Phone    string `json:"phone"`
	Img      string `json:"img"`
}

//DriverOrderDataOffer - offer data
type DriverOrderDataOffer struct {
	IDDriver string           `json:"iddriver"`
	IDUser   string           `json:"iduser"`
	Name     string           `json:"username"`
	Phone    string           `json:"phone"`
	Duration int              `json:"duration"`
	Rating   int              `json:"rating"`
	Img      string           `json:"img"`
	Lat      float64          `json:"lat"`
	Lon      float64          `json:"lon"`
	Car      CarOrderData     `json:"car"`
	Partner  PartnerOrderData `json:"partner"`
}

//CarOrderData
type CarOrderData struct {
	Mark   string    `json:"mark"`
	Color  ColorAuto `json:"color"`
	Number string    `json:"reg-number"`
}

//PartnerOrderData
type PartnerOrderData struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

//ColorAuto
type ColorAuto struct {
	Title string `json:"title"`
	Value string `json:"value"`
}
