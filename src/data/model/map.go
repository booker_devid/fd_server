package model

//MapNoteData - Data errors on map
type MapNoteData struct {
	Token   string  `json:"token"`
	Comment string  `json:"comment"`
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
}

//LocationModel -
type LocationModel struct {
	Type     string          `json:"type"`
	Licence  string          `json:"licence"`
	Features []FeaturesModel `json:"features"`
}

//FeaturesModel - for LocationModel
type FeaturesModel struct {
	Type       string          `json:"type"`
	Properties PropertiesModel `json:"properties"`
	Bbox       []float64       `json:"bbox"`
	Geometry   GeometryModel   `json:"geometry"`
}

//PropertiesModel - For FeaturesModel
type PropertiesModel struct {
	PlaceID     int64       `json:"place_id"`
	OsmType     string      `json:"osm_type"`
	OsmID       int64       `json:"osm_id"`
	PlaceRank   int64       `json:"place_rank"`
	Category    string      `json:"category"`
	Type        string      `json:"type"`
	Importance  int64       `json:"importance"`
	Addresstype string      `json:"addresstype"`
	Name        string      `json:"name"`
	DisplayName string      `json:"display_name"`
	Address     interface{} `json:"address"`
}

//GeometryModel - for GeometryModel
type GeometryModel struct {
	Type        string    `json:"type"`
	Coordinates []float64 `json:"coordinates"`
}

//PlaceDataModel  - for ClientApp take data for place
type PlaceDataModel struct {
	Name        string `json:"nme"`
	Housenumber string `json:"hon"`
	Street      string `json:"str"`
	City        string `json:"cty"`
	Lat         string `json:"lat"`
	Lon         string `json:"lon"`
}

//AllNameAddrForSearchRequest - for Client take list addreses for search request
type AllNameAddrForSearchRequest struct {
	Addr []PlaceDataModel `json:"plc"`
}

//OneUniversal - for others request and respons ...
type OneUniversal struct {
	Value string `json:"val"`
}

//TrueAddress - Checking addres
type TrueAddress struct {
	Addr string  `json:"str"`
	Lat  float64 `json:"lat"`
	Lon  float64 `json:"lon"`
}

//FeaturesModelObject - for LocationModelSearch
type FeaturesModelObject struct {
	PlaceID     int64    `json:"place_id"`
	Licence     string   `json:"licence"`
	OsmType     string   `json:"osm_type"`
	OsmID       int64    `json:"osm_id"`
	Boundbox    []string `json:"boundingbox"`
	Lat         string   `json:"lat"`
	Lon         string   `json:"lon"`
	DisplayName string   `json:"display_name"`
	Class       string   `json:"class"`
	Type        string   `json:"type"`
	Importance  float64  `json:"importance"`
}

//PropertiesModelSearch - For FeaturesModelSearch
type PropertiesModelSearch struct {
	DisplayName string `json:"display_name"`
	PlaceRank   int64  `json:"place_rank"`
	Category    string `json:"category"`
	Type        string `json:"type"`
	Importance  int64  `json:"importance"`
}

//LocationModelCheck - loading data of addreses
type LocationModelCheck struct {
	Type     string               `json:"type"`
	Features []FeaturesModelCheck `json:"features"`
}

//FeaturesModelCheck - for LocationModelSearch
type FeaturesModelCheck struct {
	Type       string               `json:"type"`
	Properties PropertiesModelCheck `json:"properties"`
	Bbox       []float64            `json:"bbox"`
	Geometry   GeometryModel        `json:"geometry"`
}

//PropertiesModelCheck - For FeaturesModelSearch
type PropertiesModelCheck struct {
	PlaceID     int64  `json:"place_id"`
	OsmType     string `json:"osm_type"`
	OsmID       int64  `json:"osm_id"`
	DisplayName string `json:"display_name"`
	PlaceRank   int64  `json:"place_rank"`
	Category    string `json:"category"`
	Type        string `json:"type"`
	Importance  int64  `json:"importance"`
	Icon        string `json:"icon"`
}
