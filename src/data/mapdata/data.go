package mapdata

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"../../utility"
	"../model"
)

//Client - struct for API request
type Client struct {
	httpClient *http.Client
}

//Block Try Catch
type Block struct {
	Try     func()
	Catch   func(Exception)
	Finally func()
}

//Exception - good interface
type Exception interface{}

//Throw - Up exeption
func Throw(up Exception) {
	panic(up)
}

//Do RunBlock
func (tcf Block) Do() {
	if tcf.Finally != nil {
		defer tcf.Finally()
	}
	if tcf.Catch != nil {
		defer func() {
			if r := recover(); r != nil {
				tcf.Catch(r)
			}
		}()
	}
	tcf.Try()
}

//TakeAddrOnCoordinates - take data on coordinate by addr of house (Nizhnevartovsk)
func TakeAddrOnCoordinates(lat, lon string) (model.PlaceDataModel, error) {
	url := fmt.Sprintf("https://nominatim.openstreetmap.org/reverse?format=geojson&lat=%v&lon=%v", lat, lon)
	client := Client{}
	client.httpClient = &http.Client{}
	dataModel := model.LocationModel{}
	placeModel := model.PlaceDataModel{}
	res, err := client.httpClient.Get(url)
	if err != nil {
		return placeModel, utility.ErrorHandler(" TakeAddrOnCoordinates.Get ", err)
	}
	defer client.httpClient.CloseIdleConnections()
	Block{
		Try: func() {
			if res.Body != nil {
				json.NewDecoder(res.Body).Decode(&dataModel)
				addresData := dataModel.Features[0].Properties.Address.(map[string]interface{})
				fmt.Println("all dataa - ", addresData)
				if addresData["address29"] != nil && addresData["address29"] != "<nil>" {
					placeModel.Name = fmt.Sprintf("%v", addresData["address29"])
				} else if addresData["mall"] != nil && addresData["mall"] != "<nil>" {
					placeModel.Name = fmt.Sprintf("%v", addresData["mall"])
				} else if addresData["school"] != nil && addresData["school"] != "<nil>" {
					placeModel.Name = fmt.Sprintf("%v", addresData["school"])
				} else if addresData["kindergarten"] != nil && addresData["kindergarten"] != "<nil>" {
					placeModel.Name = fmt.Sprintf("%v", addresData["kindergarten"])
				} else if addresData["college"] != nil && addresData["college"] != "<nil>" {
					placeModel.Name = fmt.Sprintf("%v", addresData["colleg"])
				} else if addresData["pedestrian"] != nil && addresData["pedestrian"] != "<nil>" {
					placeModel.Name = fmt.Sprintf("%v", addresData["pedestrian"])
				} else if addresData["community_centre"] != nil && addresData["community_centre"] != "<nil>" {
					placeModel.Name = fmt.Sprintf("%v", addresData["community_centre"])
				} else {
					placeModel.Name = ""
				}
				if addresData["house_number"] != nil {
					placeModel.Housenumber = fmt.Sprintf("%v", addresData["house_number"])
				} else {
					placeModel.Housenumber = ""
				}
				if addresData["road"] != nil {
					placeModel.Street = fmt.Sprintf("%v", addresData["road"])
				} else {
					placeModel.Street = ""
				}
				if addresData["city"] != nil {
					fmt.Println("Read from open-street -", addresData["city"])
					placeModel.City = fmt.Sprintf("%v", addresData["city"])
				} else if addresData["town"] != nil {
					fmt.Println("Read from open-street -", addresData["town"])
					placeModel.City = fmt.Sprintf("%v", addresData["town"])
				}
				placeModel.Lat = lat
				placeModel.Lon = lon
				if placeModel.Housenumber == "" {
					placeModel.Name = ""
					placeModel.Housenumber = ""
					placeModel.Street = ""
					placeModel.City = ""
				}
			} else {
				placeModel.Name = ""
				placeModel.Housenumber = ""
				placeModel.Street = ""
				placeModel.City = ""
			}
		},
		Catch: func(e Exception) {
			placeModel.Name = ""
			placeModel.Housenumber = ""
			placeModel.Street = ""
			placeModel.City = ""
		},
	}.Do()
	return placeModel, nil
}

//SearchAddrOnWord - take data on coordinate by addr of house (Nizhnevartovsk)
func SearchAddrOnWord(city, cityName, addr string) (model.AllNameAddrForSearchRequest, error) {
	checkAddrExistInList := false
	url := fmt.Sprintf("https://nominatim.openstreetmap.org/search.php?format=json&city=%v&street=%v", city, addr)
	client := Client{}
	client.httpClient = &http.Client{}
	dataModel := []model.FeaturesModelObject{}
	addrModel := model.AllNameAddrForSearchRequest{}
	res, err := client.httpClient.Get(url)
	if err != nil {
		return addrModel, utility.ErrorHandler(" TakeAddrOnCoordinates.Get ", err)
	}
	defer client.httpClient.CloseIdleConnections()
	fmt.Println("City search: ", cityName)
	Block{
		Try: func() {
			if res.Body != nil {
				json.NewDecoder(res.Body).Decode(&dataModel)
				for _, addr := range dataModel {
					place, _ := TakeAddrOnCoordinates(addr.Lat, addr.Lon)
					checkAddrExistInList = false
					for _, plc := range addrModel.Addr {
						if plc.Housenumber == place.Housenumber {
							if plc.Street == place.Street {
								checkAddrExistInList = true
								break
							}
						}
					}
					fmt.Println(place)
					if checkAddrExistInList != true {
						if place.Housenumber == "" {
							continue
						}
						if place.Street == "" || place.City == "" || place.City != cityName {
							continue
						}
						addrModel.Addr = append(addrModel.Addr, place)
					}
				}
			} else {
				addrModel.Addr = nil
			}
		},
		Catch: func(e Exception) {
			addrModel.Addr = nil
		},
	}.Do()
	return addrModel, nil
}

//CheckAddres - take data on coordinate by addr of house (Nizhnevartovsk)
func CheckAddres(city, addr string) (model.TrueAddress, error) {
	url := fmt.Sprintf("https://nominatim.openstreetmap.org/search.php?city=%v&street=%v&format=geojson", city, addr)
	client := Client{}
	client.httpClient = &http.Client{}
	dataModel := model.LocationModelCheck{}
	addrModel := model.TrueAddress{}
	res, err := client.httpClient.Get(url)
	if err != nil {
		return addrModel, utility.ErrorHandler(" TakeAddrOnCoordinates.Get ", err)
	}
	defer client.httpClient.CloseIdleConnections()
	Block{
		Try: func() {
			if res.Body != nil {
				json.NewDecoder(res.Body).Decode(&dataModel)
				fmt.Println(dataModel)
				addrModel.Lon = dataModel.Features[0].Geometry.Coordinates[0] //Take lon
				addrModel.Lat = dataModel.Features[0].Geometry.Coordinates[1] //Take lat
				houseNumber := 0
				buildNumber := 0
				houseChar := ""
				street := ""
				check := false
				addr := strings.Split(dataModel.Features[0].Properties.DisplayName, ",")
				if len(addr) <= 0 {
					check = true
					addrModel.Addr = ""
				}
				fmt.Sscanf(addr[0], "%d", &houseNumber)
				if houseNumber != 0 && check == false {
					houseNumber = 0
					fmt.Sscanf(addr[0], "%d/%d", &houseNumber, &buildNumber)
					if buildNumber != 0 && houseNumber != 0 && check == false {
						street = addr[1]
						addrModel.Addr = fmt.Sprintf("%v, %v/%v", street, houseNumber, buildNumber) //Take addr
						check = true
					}
					houseNumber = 0
					fmt.Sscanf(addr[0], "%d%s", &houseNumber, &houseChar)
					if houseChar != "" && houseChar != " " && houseNumber != 0 && check == false {
						street = addr[1]
						addrModel.Addr = fmt.Sprintf("%v, %v%v", street, houseNumber, houseChar) //Take addr
						check = true
					}
					fmt.Sscanf(addr[0], "%d", &houseNumber)
					street = addr[1]
					addrModel.Addr = fmt.Sprintf("%v, %v", street, houseNumber) //Take addr
				} else {
					street = addr[0]
					addrModel.Addr = fmt.Sprintf(" %v", street) //Take addr
				}
			}
		},
		Catch: func(e Exception) {
			addrModel.Addr = ""
		},
	}.Do()
	return addrModel, nil
}
