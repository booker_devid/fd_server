package database

/*
Packet database for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
*/

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"../../utility"
	"../model"
)

/*LoadListCities - load list of active cities for client*/
func (db *Database) LoadListCities() ([]byte, error) {
	citiesJSON := model.City{}
	modelCity := model.CityModel{}
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT regions.region || ' ' ||  cities.city AS res, cities.en_city, cities.city, cities.city_lat, cities.city_lon, cities.min_price FROM regions, cities WHERE regions.id = cities.region")
	for ExQue.Next() {
		ExQue.Scan(&modelCity.NameCity, &modelCity.NameCityEn, &modelCity.NameCitySh, &modelCity.Lat, &modelCity.Lon, &modelCity.Price)
		citiesJSON.City = append(citiesJSON.City, modelCity)
	}
	data, err := json.Marshal(citiesJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListCities.Marshal", err)
	}
	return data, nil
}

/*CityNameByEnCityName - load name city form database where en name equals sityEn*/
func (db *Database) CityNameByEnCityName(cityEn string) (bool, string, error) {
	if len(cityEn) == 0 {
		return false, "", nil
	}
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return false, "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	var city string
	dbConn.QueryRow(context.Background(), "SELECT city FROM cities WHERE en_city = $1", cityEn).Scan(&city)
	fmt.Println("scan.city: ", city)
	if len(city) == 0 {
		return false, city, nil
	}
	return true, city, nil
}

/*LoadListAllCities - load list of all cities*/
func (db *Database) LoadListAllCities() ([]byte, error) {
	citiesJSON := model.City{}
	modelCity := model.CityModel{}
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT regions.region || ' ' ||  res.city, res.en_city, res.city, res.city_lat, res.city_lon, res.min_price FROM regions, (SELECT city, region, en_city, city_lat, city_lon, min_price FROM cities WHERE active = true) AS res WHERE regions.id = res.region")
	for ExQue.Next() {
		ExQue.Scan(&modelCity.NameCity, &modelCity.NameCityEn, &modelCity.NameCitySh, &modelCity.Lat, &modelCity.Lon, &modelCity.Price)
		citiesJSON.City = append(citiesJSON.City, modelCity)
	}
	data, err := json.Marshal(citiesJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListCities.Marshal", err)
	}
	return data, nil
}

//TakeIDCity - take id city from database
func (db *Database) TakeIDCity(city string) (string, error) {
	var idCity string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM (SELECT cities.id, regions.region || ' ' ||  cities.city AS res, cities.city FROM regions, cities WHERE regions.id = cities.region) AS res WHERE res = $1 OR res.city = $1",
		city).Scan(&idCity); err != nil {
		return "", utility.ErrorHandler(errorPQX+": TakeIDCity! ", err)
	}
	return idCity, nil
}

//GetOrderInCity
func (db *Database) GetOrderInCity(idCity string) (model.ActionCreateOrdersData, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.QueryRow(context.Background(), "SELECT  FROM ")
	modelCreateOrdersData := model.ActionCreateOrdersData{}
	modelCreateOrdersData.Type = "orders"
	modelCreateOrdersData.Object = "created"
	var dataPlaces []byte
	var img bool
	orders, err := dbConn.Query(context.Background(),
		"SELECT tr.id, us.username, us.phone, us.img, us.rating, tr.status_trip, tr.in_city, tr.places, tr.price, tr.babych, tr.smoking, tr.fastup, tr.comment, tr.animal, tr.plane, tr.date_plane, tr.pay FROM users AS us, trip_client AS tr WHERE us.id = tr.client AND tr.city = $1 AND tr.status_trip = 'created'", idCity)
	if err != nil {
		return modelCreateOrdersData, nil
	}
	for orders.Next() {
		order := model.OrderCreatedData{}
		option := model.OptionData{}
		orders.Scan(&order.ID, &order.Name, &order.Phone, &img, &order.Rating, &order.Status, &order.InCity, &dataPlaces, &option.Price, &option.Babych, &option.Smoking, &option.FastUp, &option.Comment, &option.Animal, &option.PlanOrder, &option.PlanTime, &option.SystemPay)
		json.Unmarshal(dataPlaces, &order.Places)
		for idx, place := range order.Places {
			fmt.Printf("\nPlace %v:\n%v\nLat - %v, Lon - %v\n", idx, place.Addr, place.Lat, place.Lon)
		}
		order.AddOption = option
		if img {
			order.Img = fmt.Sprintf("%v%v", "https://scar.metro86.ru:4740/media/usr?phone=", order.Phone)
		} else {
			order.Img = ""
		}
		//Preparation data
		fmt.Println("+ one order")
		modelCreateOrdersData.Data = append(modelCreateOrdersData.Data, order)
	}
	return modelCreateOrdersData, err
}

//GetLastOrderInCity
func (db *Database) GetLastOrderInCity(idCity string) (int64, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var unix int64
	if err := dbConn.QueryRow(context.Background(), "SELECT MAX(date_start) FROM trip_client WHERE city = $1", idCity).Scan(&unix); err != nil {
		if err := dbConn.QueryRow(context.Background(), "SELECT MAX(date_start) FROM orders WHERE city = $1", idCity).Scan(&unix); err != nil {
			return 0, nil
		}
	}
	return (time.Now().Unix() - unix) / 60, nil
}

//TakeIDCityOnIDUser - take id city from database
func (db *Database) TakeIDCityOnIDUser(idUser string) (string, error) {
	var idCity string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT city FROM users WHERE id = $1",
		idUser).Scan(&idCity); err != nil {
		return "", utility.ErrorHandler(errorPQX+": TakeIDCity! ", err)
	}
	return idCity, nil
}

//LoadListColor - load list of color for client
func (db *Database) LoadListColor() ([]byte, error) {
	var loadColor model.Properties
	colorsJSON := make([]model.Properties, 0)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT id, color, code FROM color_auto")
	for ExQue.Next() {
		ExQue.Scan(&loadColor.ID, &loadColor.Value, &loadColor.Code)
		colorsJSON = append(colorsJSON, loadColor)
	}
	data, err := json.Marshal(colorsJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListColor.Marshal", err)
	}
	return data, nil
}

//TakeIDColor - take id color from database
func (db *Database) TakeIDColor(color string) (string, error) {
	var idColor string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM color_auto WHERE color = $1",
		color).Scan(&idColor); err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return idColor, nil
}

//LoadListMark - load list of mark for client
func (db *Database) LoadListMark() ([]byte, error) {
	var loadMark model.Properties
	markJSON := make([]model.Properties, 0)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT id, mark FROM mark")
	for ExQue.Next() {
		ExQue.Scan(&loadMark.ID, &loadMark.Value)
		markJSON = append(markJSON, loadMark)
	}
	data, err := json.Marshal(markJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListMark.Marshal", err)
	}
	return data, nil
}

//TakeIDMark - take id mark from database
func (db *Database) TakeIDMark(mark string) (string, error) {
	var idMark string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM mark WHERE mark = $1",
		mark).Scan(&idMark); err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return idMark, nil
}

//TakeIDUser -
func (db *Database) TakeIDUser(phone string) (string, error) {
	var idUser string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM users WHERE phone = $1",
		phone).Scan(&idUser); err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return idUser, nil
}

//WriteErrorOnMapInDatabase - Write error on map in database
func (db *Database) WriteErrorOnMapInDatabase(idUser string, mapData model.MapNoteData) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	dbConn.Exec(context.Background(), "INSERT INTO map_note(client, comment, lat, lon) VALUES($1,$2,$3,$4)", idUser, mapData.Comment, mapData.Lat, mapData.Lon)
	return err
}

//CarVerificationInDatabase  - set data in database in value verification in car table
func (db *Database) CarVerificationInDatabase(idCar string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idUser string
	dbConn.QueryRow(context.Background(), "UPDATE cars SET verification = TRUE WHERE owner_user = $1", idCar).Scan(&idCar)
	fmt.Println("Owner - ", idUser)
	dbConn.Exec(context.Background(), "SELECT pg_notify('service','verification_car*'||(SELECT fb_token FROM tokens WHERE id = $1)||'*true*Автомобиль был успешно подтвержден!')", idUser)
	return nil
}

//DriverVerificationInDatabase  - set data in database in value verification in driver table
func (db *Database) DriverVerificationInDatabase(idDriver string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err := dbConn.QueryRow(context.Background(), "UPDATE drivers SET verification_driver = TRUE WHERE id = $1", idDriver).Scan(idDriver); err != nil {
		return err
	}
	dbConn.Exec(context.Background(), "pg_notify('service','verification_driver*'||(SELECT fb_token FROM tokens WHERE id = $1))||'*true*Данные были подтверждены, теперь вы можете переключиться в режим водителя!'", idDriver)
	return nil
}

//DataPlacesInOrder
func (db *Database) DataPlacesInOrder(idOrder string) ([]model.PlaceData, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var modelPlaces []model.PlaceData
	var data []byte
	if err := dbConn.QueryRow(context.Background(), "SELECT places FROM trip_client WHERE id = $1", idOrder).Scan(&data); err != nil {
		return modelPlaces, err
	}
	err := json.Unmarshal(data, &modelPlaces)
	if err != nil {
		return modelPlaces, err
	}
	return modelPlaces, nil
}
