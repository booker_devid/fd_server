package database

/*
Packet database for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
*/

import (
	"encoding/json"
	"fmt"
	"net/http"

	"../../utility"
	"../mapdata"
	"../model"
)

/*AdminRepo - interface for admin*/
var errorRepo = "Repository error# "

//ClientRepo - is a interface using for client
type ClientRepo interface {
	LoadListCities() ([]byte, error)
	LoadListAllCities() ([]byte, error)
	CityNameByEnCityName(cityEn string) (bool, string, error)
	LoadListColor() ([]byte, error)
	TakeIDUser(phone string) (string, error)
	TakeIDMark(mark string) (string, error)
	TakeIDColor(color string) (string, error)
	TakeIDCityOnIDUser(idUser string) (string, error)
	TakeIDCity(city string) (string, error)
	CheckRegistrationToken(token string) (bool, string, error)
	CheckToken(token string) (bool, string, error)
	CheckCity(city string) bool
	WriteErrorOnMapInDatabase(idUser string, mapData model.MapNoteData) error
	CarVerificationInDatabase(idCar string) error
	DriverVerificationInDatabase(idDriver string) error
	DataPlacesInOrder(idOrder string) ([]model.PlaceData, error)
	SetRatingUser(idUser string, rating bool) error
}

//DriverRepo - is a interface using for driver
type DriverRepo interface {
	LoadListColor() ([]byte, error)
	LoadListMark() ([]byte, error)
}

//Order - is a interface using for driver
type Order interface {
	// CreateOrder(idUser string, dataOrderForSend *model.OrderModelForSend) (string, error)
	LoadListMark() ([]byte, error)
	CreateOffer(idUser string, dataOffer *model.OfferDriverData) error
	CreateRecovery(idUser, idOrder string)
	CreateCancel(idUser, idOrder string)
	GetLastOrderInCity(idCity string) (int64, error)
	GetOrderInCity(idCity string) (model.ActionCreateOrdersData, error)
}

//TestModelClientOrder -
func TestModelClientOrder() []byte {
	modelClient := model.SocketClient{}
	place := model.Place{}
	modelClient.Order.Places = append(modelClient.Order.Places, place)
	data, _ := json.Marshal(modelClient)
	return data
}

//TakeCityIDOnNameCity - take id cituy from database
func TakeCityIDOnNameCity(repo ClientRepo, nameCity string) (string, error) {
	return repo.TakeIDCity(nameCity)
}

//TakeCityIDOnIDOrder -
func TakeCityIDOnIDOrder(repo ClientRepo, idOrder string) (string, error) {
	Places, err := repo.DataPlacesInOrder(idOrder)
	if err != nil {
		return "", err
	}
	return repo.TakeIDCity(Places[0].City)
}

//TakeCityName - take Id city on id user
func TakeCityName(repo ClientRepo, idUser string) (string, error) {
	return repo.TakeIDCityOnIDUser(idUser)
}

//CheckingUserToken - Cheking user id by token
func CheckingUserToken(repo ClientRepo, token string) (bool, string, error) {
	return repo.CheckToken(token)
}

//ListCities - back user data
func ListCities(repo ClientRepo) ([]byte, error) {
	return repo.LoadListAllCities()
}

//ListAllCities - back user data
func ListAllCities(repo ClientRepo) ([]byte, error) {
	return repo.LoadListCities()
}

//CheckCityEn - Loaf name city for checking in geo_modul
func CheckCityEn(repo ClientRepo, cityEn string) (bool, string, error) {
	return repo.CityNameByEnCityName(cityEn)
}

//ListСolorsAuto - back user data
func ListСolorsAuto(repo DriverRepo) ([]byte, error) {
	return repo.LoadListColor()
}

//ListMarksAuto - back user data
func ListMarksAuto(repo DriverRepo) ([]byte, error) {
	return repo.LoadListMark()
}

//TestRepo - take of data after success authorization
func TestRepo(repo ClientRepo) error {
	fmt.Println("Check repo!")
	return nil
}

//ListWocketsUP - List up websocket
func LoadOrders(db *Database, idCity string) ([]byte, error) {
	modelWorkModelSend := model.WorkModelSend{}
	modelWorkModelSend.Type = "orders"
	orders, err := db.GetOrderInCity(idCity)
	if len(orders.Data) == 0 || err != nil {
		unix, _ := db.GetLastOrderInCity(idCity)
		if unix > 1440 {
			unix = 1440
		}
		modelWorkModelSend.Object = "empty"
		modelWorkModelSend.Data = model.ValueData{Value: int(unix)}
		return json.Marshal(modelWorkModelSend)
	}
	modelWorkModelSend.Object = "load"
	modelWorkModelSend.Data = orders.Data
	return json.Marshal(modelWorkModelSend)
}

//SearchAddr - search addr on word or streed
func SearchAddr(cityEn, city, addr string) (model.AllNameAddrForSearchRequest, error) {
	modelFoundAddress := model.AllNameAddrForSearchRequest{}
	fmt.Println("Поиск по запросу: ", addr)
	modelFoundAddress, err := mapdata.SearchAddrOnWord(cityEn, city, addr)
	return modelFoundAddress, err
}

//TakeInfoAddr - taking information from coordinates
func TakeInfoAddr(repo ClientRepo, lat, lon string) (model.PlaceDataModel, error) {
	addr, err := mapdata.TakeAddrOnCoordinates(lat, lon)
	if _, err := repo.TakeIDCity(addr.City); err != nil {
		addr.City = "unknow"
		return addr, err
	}
	return addr, err
}

//MapError -
func MapError(repo ClientRepo, ctx *http.Request) error {
	modelMap := model.MapNoteData{}
	err := json.NewDecoder(ctx.Body).Decode(&modelMap)
	if err != nil {
		return utility.ErrorHandler(errorJSON+": GetDriverDataOnToken.NewDecoder ", err)
	}
	fmt.Println("data map  - ", modelMap.Token, modelMap.Comment)
	_, idUser, err := repo.CheckToken(modelMap.Token)
	if err != nil {
		return utility.ErrorHandler(errorJSON+": GetDriverDataOnToken.CheckToken ", err)
	}
	return repo.WriteErrorOnMapInDatabase(idUser, modelMap)
}

//CheckAddr - search addr on word or streed
func CheckAddr(city string, ctx *http.Request) (model.TrueAddress, error) {
	modelReadData := model.OneUniversal{}
	modelFoundAddress := model.TrueAddress{}
	err := json.NewDecoder(ctx.Body).Decode(&modelReadData)
	if err != nil {
		return modelFoundAddress, utility.ErrorHandler(errorJSON+": UpdateUserData.NewDecoder ", err)
	}
	fmt.Println("Поиск по запросу: ", modelReadData.Value)
	street, err := mapdata.CheckAddres(city, modelReadData.Value)
	return street, err
}

//CarVerification - verification car
func CarVerification(repo ClientRepo, idCar string) error {
	return repo.CarVerificationInDatabase(idCar)
}

//DriverVerification - varification driver
func DriverVerification(repo ClientRepo, idDriver string) error {
	return repo.DriverVerificationInDatabase(idDriver)
}

//RatingOrder -
func RatingOrder(repo ClientRepo, token, rating string) error {
	_, idUser, err := repo.CheckToken(token)
	if err != nil {
		return utility.ErrorHandler(errorJSON+": GetDriverDataOnToken.CheckToken ", err)
	}
	if rating == "t" {
		return repo.SetRatingUser(idUser, true)
	} else {
		return repo.SetRatingUser(idUser, false)
	}
}

// //RecoveryOrderInBusAction - recovery order in
// func RecoveryOrderInBusAction(repo ClientRepo, idOrder string) error {
// 	// return repo.CreateNotifyRecoveryOrder(idOrder)
// }
