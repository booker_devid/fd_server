package database

/*
Packet database for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
*/

import (
	"context"
	"crypto/md5"
	"fmt"
	"io"

	"../../utility"
	pgxp "github.com/jackc/pgx/pgxpool"
)

var (
	errorPQX     = "Database driver PGX# "
	errorListen  = "Database modul Listen error# "
	errorActHand = "Database modul ActHandlers error# "
	errorJSON    = "Database modul JSON error# "
)

/*
Database is type for interaction with local database. She has methods initialization, opening connection and closing connection.
Fileds:
connectionStrPG string - config URI string for connection to PostgreSQL database
dbConnectPool   *pgx.ConnPool - keep pool connection to PostgreSQL database, for keep one connects use pool.Acquire() (conn, error)
*/
type Database struct {
	connectionStrPG string
	dbConnectPool   *pgxp.Pool
	workerOrder     *WorkersObject
}

//DBInstance - return of instance database
func DBInstance(connStrPG string) *Database {
	return &Database{connectionStrPG: connStrPG}
}

//CheckToken - checking a user token on actuality
func (db *Database) CheckToken(token string) (bool, string, error) {
	var idUser string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	rowFunction := dbConn.QueryRow(context.Background(), "SELECT id FROM tokens WHERE token = $1", token)
	if err = rowFunction.Scan(&idUser); err != nil {
		return false, "", err
	}
	fmt.Println("Id user: ", idUser)
	return true, idUser, nil
}

//TakeMD5Sol - Sol for token
func takeMD5Sol(idUser string) string {
	hash := md5.New()
	io.WriteString(hash, fmt.Sprintf("%v,%v", "BigBrotherSeeYou", idUser))
	return fmt.Sprintf("%x", hash.Sum(nil))
}

/*CheckRegistrationToken  - return data:
- phone
*/
func (db *Database) CheckRegistrationToken(token string) (bool, string, error) {
	var scanPhone string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return false, "", err
	}
	rowFunction := dbConn.QueryRow(context.Background(), "SELECT id FROM rtokens WHERE token = $1", token)
	if err := rowFunction.Scan(&scanPhone); err != nil {
		return false, "", err
	}
	return true, scanPhone, nil
}

//InitDatabaseConnections - opening connection to database
func (db *Database) InitDatabaseConnections() error {
	confConn, err := pgxp.ParseConfig(db.connectionStrPG) // Example: user=jack password=secret host=host1,host2,host3 port=5432,5433,5434 dbname=mydb sslmode=verify-ca
	if err != nil {                                       // https://www.postgresql.org/docs/11/libpq-connect.html#LIBPQ-MULTIPLE-HOSTS
		return utility.ErrorHandler(errorPQX+" ParseURI ", err)
	}
	db.dbConnectPool, err = pgxp.ConnectConfig(context.Background(), confConn)
	if err != nil {
		return utility.ErrorHandler(errorPQX+" NewConnPool ", err)
	}
	db.workerOrder = NewWorkerObject()
	return nil
}

//CheckCity - cheking city in database
func (db *Database) CheckCity(city string) bool {
	var enCity string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	if err != nil {
		return false
	}
	defer dbConn.Release()
	dbConn.QueryRow(context.Background(), "SELECT en_city FROM cities WHERE en_city = $1", city).Scan(&enCity)
	if enCity != city {
		return false
	}
	return true
}
