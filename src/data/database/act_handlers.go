package database

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"../../utility"
	"../model"
	pgx "github.com/jackc/pgx"
)

const URLFULLDRIVER = "https://fulldriver.su:4777"

//BusHandlersVerificationAction - Handler for other actions is not order handler
func (db *Database) BusHandlersVerificationAction(errCh chan<- string, act chan<- interface{}, localkey string) {
	pgconfListenString := strings.Split(db.connectionStrPG, "&")
	confConnListen, err := pgx.ParseConfig(pgconfListenString[0])
	dbl, err := pgx.ConnectConfig(context.Background(), confConnListen)
	if err != nil {
		errCh <- utility.ErrorHandler(errorPQX+" NewListen Verification ", err).Error()
		return
	}
	SyncTimingCities(dbl, db.workerOrder)
	timingCity := db.workerOrder.GetTimingCity()
	queryOrders, err := dbl.Query(context.Background(), "SELECT tr.id, tr.status_trip, ct.city FROM trip_client AS tr, cities AS ct WHERE ct.id = tr.city AND tr.status_trip != 'end' AND tr.srv = $1", localkey)
	for queryOrders.Next() {
		var idOrder, trip_status, city string
		queryOrders.Scan(&idOrder, &trip_status, &city)
		fmt.Println("Add order - ", idOrder, trip_status, city)
		switch trip_status {
		case "created":
			db.workerOrder.SetStepOrdersWithStateCreated(idOrder, time.Now().Unix()+int64(timingCity[city].timeLiveOrderCreated), city)
		case "start":
			db.workerOrder.SetStepOrdersWithStateStart(idOrder, time.Now().Unix()+int64(timingCity[city].timeLiveOrderCreated), city)
		case "driver_ready":
			db.workerOrder.SetStepOrdersWithStateDriverReady(idOrder, time.Now().Unix()+int64(timingCity[city].timeLiveOrderCreated), city)
		case "begin_trip":
			db.workerOrder.SetStepOrdersWithStateBeginTrip(idOrder, time.Now().Unix()+int64(timingCity[city].timeLiveOrderCreated), city)
		}
	}
	for {
		_, err = dbl.Exec(context.Background(), "LISTEN service")
		if err != nil {
			errCh <- utility.ErrorHandler(errorPQX+": ACQUIR! Error listen database ", err).Error()
			return
		}
		fmt.Println("Start listen @ServiceAction")
		for {
			notification, err := dbl.WaitForNotification(context.Background())
			if err != nil {
				errCh <- err.Error()
				break
			} else {
				fmt.Printf("\nGet notification:\n%v\n%v\n%v\n", notification.PID, notification.Channel, notification.Payload)
				go statusSigatureForService(db, notification.Payload)
			}
		}
		fmt.Println("End listen")
	}
}

func statusSigatureForService(db *Database, notifyData string) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		fmt.Println(utility.ErrorHandler(" PGQX!.PreparationDataActionCreated.Acquire ", err).Error())
	}
	status := strings.Split(notifyData, "*")
	switch status[0] {
	case "update-timing":
		fmt.Println("Synchronithation time")
		SyncTimingCities(dbConn.Conn(), db.workerOrder)
	}
	return
}

//BusHandlersOrderAction - function for listen database on action:
func (db *Database) BusHandlersOrderAction(errCh chan<- string, act chan<- interface{}) {
	pgconfListenString := strings.Split(db.connectionStrPG, "&")
	confConnListen, err := pgx.ParseConfig(pgconfListenString[0])
	dbl, err := pgx.ConnectConfig(context.Background(), confConnListen)
	if err != nil {
		fmt.Println(utility.ErrorHandler(errorPQX+" NewListen ", err).Error())
		return
	}
	for {
		_, err = dbl.Exec(context.Background(), "LISTEN orders")
		if err != nil {
			errCh <- utility.ErrorHandler(errorPQX+": ACQUIR! Error listen database ", err).Error()
			return
		}
		fmt.Println("Start listen @OrderAction")
		for {
			notification, err := dbl.WaitForNotification(context.Background())
			if err != nil {
				errCh <- err.Error()
				break
			} else {
				fmt.Printf("\nGet notification:\n%v\n%v\n%v\n", notification.PID, notification.Channel, notification.Payload)
				go statusSigatureForOrder(db, notification.Payload, act)
			}
		}
		fmt.Println("End listen")
	}
}

func statusSigatureForOrder(db *Database, notifyData string, act chan<- interface{}) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		fmt.Println(utility.ErrorHandler(" PGQX!.PreparationDataActionCreated.Acquire ", err).Error())
	}
	status := strings.Split(notifyData, "*")
	switch status[0] {
	//State 1
	case "created":
		db.PreparationDataActionCreated(dbConn.Conn(), db.workerOrder, status, act, false)
	case "replay":
		db.PreparationDataActionReplay(dbConn.Conn(), db.workerOrder, status, act)
	case "offer":
		fmt.Println("Offer driver for order")
		PreparationDataOfferOrder(dbConn.Conn(), status, act)
	// case "plan_add_driver": // add after State 4
	// 	fmt.Println("In plan order add driver")
	// case "plan_del_driver": // add after State 4
	// 	fmt.Println("In plan order del driver")
	// case "plan_out": // add after State 4 (system action)
	// 	fmt.Println("Time out plan order, create notify and start order")
	// case "switch_plan": // add after State 4
	// 	fmt.Println("Switch plan order")
	case "time_out": // add (system action)
		switch status[2] {
		case "created":
			fmt.Println("Time out created -> time_out")
			PreparationDataTimeOutOrderInStateCreated(dbConn.Conn(), status, act)
		case "plan":
			fmt.Println("Time out plan")
			PreparationDataActionPlan(dbConn.Conn(), status, act)
		case "start":
			fmt.Println("Time out start -> driver_out")
			PreparationDataTimeOutOrderInStateStart(dbConn.Conn(), status, act)
		case "driver_ready":
			fmt.Println("Time out driver_ready -> client_out")
			PreparationDataTimeOutOrderInStateDriverReady(dbConn.Conn(), status, act)
		case "begin_trip":
			fmt.Println("Time out degin_trip -> check")
			PreparationDataTimeOutOrderInStateBeginTrip(dbConn.Conn(), status, act)
		}
	//State 2
	case "start":
		fmt.Println("Add driver in order")
		db.PreparationDataActionStart(dbConn.Conn(), db.workerOrder, status, act)
	case "position_driver":
		fmt.Println("Read position")
		db.PreparationDataPositionDriver(dbConn.Conn(), db.workerOrder, status, act)
	case "position_client":
		fmt.Println("Read position")
		db.PreparationDataPositionClient(dbConn.Conn(), db.workerOrder, status, act)
	case "message":
		fmt.Println("Read message")
		db.PreparationDataMessage(dbConn.Conn(), db.workerOrder, status, act)
	case "driver_ready":
		fmt.Println("Driver ready on Place A")
		db.PreparationDataActionDriverReady(dbConn.Conn(), db.workerOrder, status, act)
	case "driver_out": // (system action)
		fmt.Println("Client is waiting <_time_waite_driver> after ready")
	case "client_out": // (system action)
		fmt.Println("Driver is waiting <_time_waite_client> after ready")
	//State 3
	case "begin_trip":
		fmt.Println("Begin trip")
		db.PreparationDataActionBeginTrip(dbConn.Conn(), db.workerOrder, status, act)
	case "check": //(system action)
		fmt.Println("Checking order <_time_check_order>")
	case "true":
		fmt.Println("Checking order <_time_check_order>")
	//State 4
	case "end": //(system action)
		fmt.Println("End trip, but no Driver and Client reviews")
		db.PreparationDataActionEnd(dbConn.Conn(), db.workerOrder, status, act)
	case "canceled":
		if len(status) == 3 {
			db.workerOrder.DelInAllSatate(status[1], true)
			fmt.Println("Status - ", status[2])
			switch status[2] {
			case "created":
				fmt.Print("Canceled created order")
				PreparationDataCancelCreatedOrder(dbConn.Conn(), status, act)
			case "plan":
				fmt.Println("Candel plan")
				PreparationDataCancelPlan(dbConn.Conn(), status, act)
			case "start":
				fmt.Println("Cancel start")
				PreparationDataCancelOrderAfterStart(dbConn.Conn(), status, act)
			case "driver_ready":
				fmt.Println("Cancel driver_ready")
				PreparationDataCancelOrderAfterStart(dbConn.Conn(), status, act)
			case "begin_trip":
				fmt.Println("Cancel begin_trip")
				PreparationDataCancelOrderAfterStart(dbConn.Conn(), status, act)
			}
		}
	case "recovery":
		if len(status) == 4 {
			switch status[3] {
			case "driver_true":
				fmt.Println("Recovery driver_out")
				RecoveryOrderDriverOut(dbConn.Conn(), db.workerOrder, status, act)
			case "client_true":
				fmt.Println("Recovery client_out")
				RecoveryOrderClientOut(dbConn.Conn(), db.workerOrder, status, act)
			case "price_up":
				fmt.Println("Recovery client_out with up price")
				RecoveryOrderPriceUP(dbConn.Conn(), db.workerOrder, status, act)
			case "true":
				RecoveryOrderCheck(dbConn.Conn(), db.workerOrder, status, act)
				fmt.Println("Recovery check")
			default:
				RecoveryOrderTimeOut(dbConn.Conn(), db.workerOrder, status, act)
				fmt.Println("Recovery time_out")
			}
		}
	}
	return
}

//BusHandlersLocalSystem - function for listen database on action:
func (db *Database) BusHandlersLocalSystem(errCh chan<- string, act chan<- interface{}, localSystem string) {
	pgconfListenString := strings.Split(db.connectionStrPG, "&")
	confConnListen, err := pgx.ParseConfig(pgconfListenString[0])
	dbl, err := pgx.ConnectConfig(context.Background(), confConnListen)
	if err != nil {
		fmt.Println(utility.ErrorHandler(errorPQX+" NewListen ", err).Error())
		return
	}
	for {
		_, err = dbl.Exec(context.Background(), fmt.Sprintf("LISTEN %v", localSystem))
		if err != nil {
			errCh <- utility.ErrorHandler(errorPQX+": ACQUIR! Error listen database ", err).Error()
			return
		}
		fmt.Println("Start listen @OrderAction")
		for {
			notification, err := dbl.WaitForNotification(context.Background())
			if err != nil {
				errCh <- err.Error()
				break
			} else {
				fmt.Printf("\nGet notification:\n%v\n%v\n%v\n", notification.PID, notification.Channel, notification.Payload)
				go statusSigatureForOrderLocalSystem(db, notification.Payload, act)
			}
		}
		fmt.Println("End listen")
	}
}

func statusSigatureForOrderLocalSystem(db *Database, notifyData string, act chan<- interface{}) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		fmt.Println(utility.ErrorHandler(" PGQX!.PreparationDataActionCreated.Acquire ", err).Error())
	}
	fmt.Println("Create local data")
	status := strings.Split(notifyData, "*")
	switch status[0] {
	case "created":
		db.PreparationDataActionCreated(dbConn.Conn(), db.workerOrder, status, act, true)
	}
}

//SyncTimingCities - Synchronithation timing cities on servers.
func SyncTimingCities(db *pgx.Conn, wo *WorkersObject) {
	CitiesQuery, _ := db.Query(context.Background(), "SELECT city.city AS full_name, city.time_live_order_created, city.time_waiting_client, city.time_live_order_trip, city.time_waiting_check, city.time_waiting_driver FROM cities AS city, regions AS reg WHERE reg.id = city.region")
	citiesForSync := make(map[string]timeCity)
	for CitiesQuery.Next() {
		tmpTiming := timeCity{}
		var idCity string
		CitiesQuery.Scan(&idCity, &tmpTiming.timeLiveOrderCreated, &tmpTiming.timeWaitingClient, &tmpTiming.timeLiveOrderTrip, &tmpTiming.timeWaitingCheck, &tmpTiming.timeWaitingDriver)
		fmt.Printf("Time in city %v - %v | %v | %v | %v | %v\n", idCity, tmpTiming.timeLiveOrderCreated, tmpTiming.timeWaitingClient, tmpTiming.timeLiveOrderTrip, tmpTiming.timeWaitingCheck, tmpTiming.timeWaitingDriver)
		citiesForSync[idCity] = tmpTiming
	}
	wo.SetTimingCity(citiesForSync)
	fmt.Println("Synchronithation time success...")
}

func createActionFromDatabase(data interface{}, act chan<- interface{}) {
	fmt.Println("Create action in database")
	act <- data
}

//PreparationDataMessage
func (db *Database) PreparationDataMessage(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	var idClient, idDriver string
	if err := dbConn.QueryRow(context.Background(), "SELECT client, driver FROM trip_client WHERE id = $1", data[1]).Scan(&idClient, &idDriver); err != nil {
		return
	}
	fmt.Println("Write driver - ", idDriver)
	fmt.Println("Write client - ", idClient)
	if data[2] == idClient {
		dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "message", Data: model.EventMessage{User: idClient, Order: data[1], Owner: true, Message: data[3]}})
		createActionFromDatabase(dataJSON, act)
		dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "message", Data: model.EventMessage{User: idDriver, Order: data[1], Owner: false, Message: data[3]}})
		createActionFromDatabase(dataJSON, act)
	} else {
		dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "message", Data: model.EventMessage{User: idClient, Order: data[1], Owner: false, Message: data[3]}})
		createActionFromDatabase(dataJSON, act)
		dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "message", Data: model.EventMessage{User: idDriver, Order: data[1], Owner: true, Message: data[3]}})
		createActionFromDatabase(dataJSON, act)
	}
}

//PreparationDataPositionClient - Cancel order
func (db *Database) PreparationDataPositionClient(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	var position model.PositionAction
	if err := dbConn.QueryRow(context.Background(), "SELECT driver FROM trip_client WHERE id = $1", data[1]).Scan(&position.User); err != nil {
		fmt.Println("dr pos - ", err.Error())
		return
	}
	position.Lat, _ = strconv.ParseFloat(data[2], 64)
	position.Lon, _ = strconv.ParseFloat(data[3], 64)
	jsonData, _ := json.Marshal(model.PositionEvent{Type: "orders", Object: "position", Data: position})
	createActionFromDatabase(jsonData, act)
}

//PreparationDataPositionDriver - Cancel order
func (db *Database) PreparationDataPositionDriver(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	var position model.PositionAction
	if err := dbConn.QueryRow(context.Background(), "SELECT client FROM trip_client WHERE id = $1", data[1]).Scan(&position.User); err != nil {
		fmt.Println("cl pos - ", err.Error())
		return
	}
	position.Lat, _ = strconv.ParseFloat(data[2], 64)
	position.Lon, _ = strconv.ParseFloat(data[3], 64)
	jsonData, _ := json.Marshal(model.PositionEvent{Type: "orders", Object: "position", Data: position})
	createActionFromDatabase(jsonData, act)
}

//PreparationDataActionReplay - Preparation data replcay order. Type list - 0.-TypeAction * 1.-ID-Order * 2.- ID-NewOrder * 3.- IDUser
func (db *Database) PreparationDataActionReplay(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	//remove old and add new worker
	listOrder := wo.GetStepOrdersWithStateCreated()
	val, ok := listOrder[data[1]]
	if !ok || val.city == "" {
		listOrder = wo.GetStepOrdersWithStateOnCancel()
		if val, ok = listOrder[data[1]]; !ok || val.city == "" {
			db.NotifyReplay(dbConn, wo, data, act)
			return
		}
	}
	timingCity := wo.GetTimingCity()
	fmt.Println("add- ", data[2], " ", time.Now().Unix()+int64(timingCity[listOrder[data[1]].city].timeLiveOrderCreated), " ", listOrder[data[1]].city)
	wo.SetStepOrdersWithStateCreated(data[2], time.Now().Unix()+int64(timingCity[listOrder[data[1]].city].timeLiveOrderCreated), listOrder[data[1]].city)
	wo.DelInAllSatate(data[1], true)
	dataListOutJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "out_list", Data: model.EventListOut{ID: data[1]}})
	createActionFromDatabase(dataListOutJSON, act)
	//create order
	db.NotifyReplay(dbConn, wo, data, act)
}

//NotifyReplay
func (db *Database) NotifyReplay(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	modelCreateOrderData := model.ActionCreateOrderData{}
	order := model.OrderCreatedData{}
	option := model.OptionData{}
	var dataPlaces []byte
	var img bool
	dbConn.QueryRow(context.Background(),
		"SELECT tr.id, us.username, us.phone, us.img, us.rating, tr.status_trip, tr.in_city, tr.places, tr.price, tr.babych, tr.smoking, tr.fastup, tr.comment, tr.animal, tr.plane, tr.date_plane, tr.pay FROM users AS us, trip_client AS tr WHERE tr.id = $1 AND us.id = tr.client",
		data[2]).Scan(&order.ID, &order.Name, &order.Phone, &img, &order.Rating, &order.Status, &order.InCity, &dataPlaces, &option.Price, &option.Babych, &option.Smoking, &option.FastUp, &option.Comment, &option.Animal, &option.PlanOrder, &option.PlanTime, &option.SystemPay)
	fmt.Printf("Data: %v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v",
		order.ID, order.Name, order.Status, order.InCity, dataPlaces, option.Price, option.Babych, option.Smoking, option.FastUp, option.Comment, option.Animal, option.PlanOrder, option.PlanTime, option.SystemPay)
	json.Unmarshal(dataPlaces, &order.Places)
	for idx, place := range order.Places {
		fmt.Printf("\nPlace %v:\n%v\nLat - %v, Lon - %v\n", idx, place.Addr, place.Lat, place.Lon)
	}
	order.AddOption = option
	if img {
		order.Img = fmt.Sprintf("%v/media/usr?phone=%v&token=%v", URLFULLDRIVER, order.Phone, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
	}
	//Preparation data
	modelCreateOrderData.Type = "orders"
	modelCreateOrderData.Object = "created"
	modelCreateOrderData.Data = order
	//send data in socket
	dataOrderJSON, _ := json.Marshal(modelCreateOrderData)
	createActionFromDatabase(dataOrderJSON, act)
}

//PreparationDataActionDriverReady - Preparation data replcay order. Type list - 0.-TypeAction * 1.-ID-Order
func (db *Database) PreparationDataActionDriverReady(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	fmt.Println("Preparation driver ready")
	listOrder := wo.GetStepOrdersWithStateStart()
	val, ok := listOrder[data[1]]
	if !ok || val.city == "" {
		listOrder := wo.GetStepOrdersWithStateDriverReady()
		if val, ok = listOrder[data[1]]; !ok || val.city == "" {
			listOrder = wo.GetStepOrdersWithStateOnCancel()
			if val, ok = listOrder[data[1]]; !ok || val.city == "" {
				db.NotifyDriverReady(dbConn, wo, data, act)
				return
			}
		}
	}
	city := val.city
	timingCity := wo.GetTimingCity()
	fmt.Println("city - ", city)
	fmt.Println("time - ", timingCity[city].timeWaitingClient)
	wo.DelInAllSatate(data[1], true)
	wo.SetStepOrdersWithStateDriverReady(data[1], time.Now().Unix()+int64(timingCity[city].timeWaitingClient), city)
	db.NotifyDriverReady(dbConn, wo, data, act)
}

//NotifyDriverReady
func (db *Database) NotifyDriverReady(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	var idClient, idDriver string
	if err := dbConn.QueryRow(context.Background(), "SELECT client, driver FROM trip_client WHERE id = $1", data[1]).Scan(&idClient, &idDriver); err != nil {
		return
	}
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "driver_ready", Data: model.EventTimeOutOrder{User: idClient, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "driver_ready", Data: model.EventTimeOutOrder{User: idDriver, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataActionBeginTrip - Preparation data replcay order. Type list - 0.-TypeAction * 1.-ID-Order
func (db *Database) PreparationDataActionBeginTrip(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	fmt.Println("Preparation Begin trip")
	listOrder := wo.GetStepOrdersWithStateDriverReady()
	val, ok := listOrder[data[1]]
	if !ok || val.city == "" {
		listOrder = wo.GetStepOrdersWithStateOnCancel()
		if val, ok = listOrder[data[1]]; !ok || val.city == "" {
			db.NotifyBeginTrip(dbConn, wo, data, act)
			return
		}
	}
	city := listOrder[data[1]].city
	timingCity := wo.GetTimingCity()
	wo.DelInAllSatate(data[1], true)
	wo.SetStepOrdersWithStateBeginTrip(data[1], time.Now().Unix()+int64(timingCity[city].timeWaitingCheck), city)
	db.NotifyBeginTrip(dbConn, wo, data, act)
}

//NotifyBeginTrip
func (db *Database) NotifyBeginTrip(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	var idClient, idDriver string
	if err := dbConn.QueryRow(context.Background(), "SELECT client, driver FROM trip_client WHERE id = $1", data[1]).Scan(&idClient, &idDriver); err != nil {
		return
	}
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "begin_trip", Data: model.EventTimeOutOrder{User: idClient, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "begin_trip", Data: model.EventTimeOutOrder{User: idDriver, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataActionEnd - Preparation data replcay order. Type list - 0.-TypeAction * 1.-ID-Order
func (db *Database) PreparationDataActionEnd(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	fmt.Println("Preparation end")
	wo.DelInAllSatate(data[1], true)
	var idClient, idDriver string
	if err := dbConn.QueryRow(context.Background(), "SELECT client, driver FROM trip_client WHERE id = $1", data[1]).Scan(&idClient, &idDriver); err != nil {
		return
	}
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "end", Data: model.EventTimeOutOrder{User: idClient, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "end", Data: model.EventTimeOutOrder{User: idDriver, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	//send fb for client and driver
}

//PreparationDataActionStart - Preparation data replcay order. Type list - 0.-TypeAction * 1.-ID-Order
func (db *Database) PreparationDataActionStart(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	listOrder := wo.GetStepOrdersWithStateCreated()
	val, ok := listOrder[data[1]]
	if !ok || val.city == "" {
		listOrder = wo.GetStepOrdersWithStateOnCancel()
		if val, ok = listOrder[data[1]]; !ok || val.city == "" {
			db.NotifyStart(dbConn, wo, data, act)
			return
		}
	}
	city := val.city
	timingCity := wo.GetTimingCity()
	wo.DelInAllSatate(data[1], true)
	driverDuration, _ := strconv.Atoi(data[2])
	driverDuration = driverDuration * 60
	wo.SetStepOrdersWithStateStart(data[1], time.Now().Unix()+int64(timingCity[city].timeWaitingDriver+driverDuration), city)
	db.NotifyStart(dbConn, wo, data, act)
}

//NotifyStart
func (db *Database) NotifyStart(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	queryUser, err := dbConn.Query(context.Background(), "SELECT us.id, us.phone, us.username, us.rating, img, CASE WHEN (tr.client = us.id) THEN TRUE ELSE FALSE END AS client FROM users AS us, (SELECT client, driver FROM trip_client WHERE id = $1) AS tr WHERE us.id = tr.client OR us.id = tr.driver", data[1])
	if err != nil {
		fmt.Println("Z1", err.Error())
		return
	}
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "out_list", Data: model.EventListOut{ID: data[1]}})
	createActionFromDatabase(dataJSON, act)
	var placesData []byte
	var inCity bool
	var idCar, cabinet string
	clientData := model.ReadyOrderModelDriver{}
	driverData := model.ReadyOrderModelClient{}
	places := []model.Place{}
	option := model.OptionData{}
	for queryUser.Next() {
		var idUser, phoneUser, nameUser string
		var ratingUser int
		var img, client bool
		queryUser.Scan(&idUser, &phoneUser, &nameUser, &ratingUser, &img, &client)
		if client {
			driverData.ID = idUser
			clientData.Actor = "driver"
			clientData.IDOrder = data[1]
			clientData.Client.IDClient = idUser
			clientData.Client.Name = nameUser
			clientData.Client.Phone = phoneUser
			clientData.Client.Rating = ratingUser
			if img {
				clientData.Client.Img = fmt.Sprintf("%v/media/usr?phone=%v&token=%v", URLFULLDRIVER, phoneUser, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
			}
		} else {
			clientData.ID = idUser
			driverData.Actor = "client"
			driverData.IDOrder = data[1]
			driverData.Driver.IDDriver = idUser
			driverData.Driver.Name = nameUser
			driverData.Driver.Phone = phoneUser
			driverData.Driver.Rating = ratingUser
			if img {
				driverData.Driver.Img = fmt.Sprintf("%v/media/usr?phone=%v&token=%v", URLFULLDRIVER, phoneUser, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
			}
		}
	}
	if err := dbConn.QueryRow(context.Background(), "SELECT places, in_city, babych, smoking, fastup, animal, comment, pay, price, duration_waiting FROM trip_client WHERE id = $1",
		data[1]).Scan(&placesData, &inCity, &option.Babych, &option.Smoking, &option.FastUp, &option.Animal, &option.Comment, &option.SystemPay, &option.Price, &driverData.Driver.Duration); err != nil {
		fmt.Println("Z2", err.Error())
		return
	}
	json.Unmarshal(placesData, &places)
	if err = dbConn.QueryRow(context.Background(),
		"SELECT us.surname || ' ' || us.username AS name, CASE WHEN (dr.car_arend != NULL AND dr.car_arend != '') THEN dr.car_arend ELSE dr.car END AS rend FROM users AS us, drivers AS dr WHERE us.id = $1 AND dr.id = us.id",
		driverData.Driver.IDDriver).Scan(&driverData.Driver.Name, &idCar); err != nil {
		fmt.Println("User data ", err.Error())
		return
	}
	if err = dbConn.QueryRow(context.Background(),
		"SELECT mk.mark, cl.color, cl.code, car.num_reg, CASE WHEN (car.owner_cabinet != NULL AND car.owner_cabinet != '') THEN car.owner_cabinet ELSE 'no' END AS cab FROM cars AS car, color_auto AS cl, mark AS mk WHERE car.id = $1 AND cl.id = car.color AND mk.id = car.mark",
		idCar).Scan(&driverData.Driver.Car.Mark, &driverData.Driver.Car.Color.Title, &driverData.Driver.Car.Color.Value, &driverData.Driver.Car.Number, &cabinet); err != nil {
		fmt.Println("Car data - ", err.Error())
		return
	}
	if len(cabinet) > 3 && cabinet != "no" {
		if err = dbConn.QueryRow(context.Background(), "SELECT id, title_ps FROM cabinet_ps WHERE id = $1",
			cabinet).Scan(&driverData.Driver.Partner.ID, &driverData.Driver.Name); err != nil {
			fmt.Println("cabinet data - ", err.Error())
			return
		}
	}
	driverData.Places = places
	clientData.Places = places
	driverData.AddOption = option
	clientData.AddOption = option
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "start", Data: driverData})
	createActionFromDatabase(dataJSON, act)
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "start", Data: clientData})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataActionCreated - Preparation data create order. Type list - 0.-TypeAction * 1.-ID-Order
func (db *Database) PreparationDataActionCreated(dbConn *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}, worker bool) { //Add
	if len(data) > 1 {
		modelCreateOrderData := model.ActionCreateOrderData{}
		order := model.OrderCreatedData{}
		option := model.OptionData{}
		var dataPlaces []byte
		var img bool
		dbConn.QueryRow(context.Background(),
			"SELECT tr.id, us.username, us.phone, us.img, us.rating, tr.status_trip, tr.in_city, tr.places, tr.price, tr.babych, tr.smoking, tr.fastup, tr.comment, tr.animal, tr.plane, tr.date_plane, tr.pay FROM users AS us, trip_client AS tr WHERE tr.id = $1 AND us.id = tr.client",
			data[1]).Scan(&order.ID, &order.Name, &order.Phone, &img, &order.Rating, &order.Status, &order.InCity, &dataPlaces, &option.Price, &option.Babych, &option.Smoking, &option.FastUp, &option.Comment, &option.Animal, &option.PlanOrder, &option.PlanTime, &option.SystemPay)
		json.Unmarshal(dataPlaces, &order.Places)
		order.AddOption = option
		if img {
			order.Img = fmt.Sprintf("%v/media/usr?phone=%v&token=%v", URLFULLDRIVER, order.Phone, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
		}
		//Preparation data
		modelCreateOrderData.Type = "orders"
		modelCreateOrderData.Object = "created"
		modelCreateOrderData.Data = order
		// fmt.Println("Новый Заказ!", fmt.Sprintf("%v %v,\n %v %v", "От", order.Places[0].Addr, "До ", order.Places[1].Addr), "\nС даными ", modelCreateOrderData, "orders")
		// Add order in worker
		if worker {
			fmt.Println("create notify created and add worker")
			timingCity := wo.GetTimingCity()
			fmt.Println("time- ", order.Places[0].City)
			wo.SetStepOrdersWithStateCreated(order.ID, time.Now().Unix()+int64(timingCity[order.Places[0].City].timeLiveOrderCreated), order.Places[0].City)
			dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "orders", fmt.Sprintf("%v*%v", order.Status, order.ID))
		} else {
			//send data in socket
			data, _ := json.Marshal(modelCreateOrderData)
			createActionFromDatabase(data, act)
		}
	}
}

//PreparationDataCancelOrder - Cancel order
func PreparationDataCancelOrder(db *pgx.Conn, data []string, act chan<- interface{}) {
	fmt.Printf("Cancel order - %v \n", data[1])
	db.Exec(context.Background(), "UPDATE trip_client SET status_trip = 'end' WHERE id = $1", data[1])
}

//PreparationDataCancelCreatedOrder - Cancel created order
func PreparationDataCancelCreatedOrder(db *pgx.Conn, data []string, act chan<- interface{}) {
	var idUser string
	fmt.Printf("Cancel order - %v \n", data[1])
	// db.Exec(context.Background(), "UPDATE trip_client SET status_trip = 'end' WHERE id = $1", data[1])
	db.QueryRow(context.Background(), "SELECT client FROM trip_client WHERE id = $1", data[1]).Scan(&idUser)
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "cancel", Data: model.EventTimeOutOrder{User: idUser, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "out_list", Data: model.EventListOut{ID: data[1]}})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataCancelPlan
func PreparationDataCancelPlan(db *pgx.Conn, data []string, act chan<- interface{}) {
	var idClient, idDriver string
	if err := db.QueryRow(context.Background(), "SELECT client, driver FROM trip_client WHERE id = $1", data[1]).Scan(&idClient, &idDriver); err != nil {
		return
	}
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "cancel", Data: model.EventTimeOutOrder{User: idClient, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "cancel", Data: model.EventTimeOutOrder{User: idDriver, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	//create send on FB
}

//PreparationDataCancelStart
func PreparationDataCancelOrderAfterStart(db *pgx.Conn, data []string, act chan<- interface{}) {
	var idClient, idDriver string
	if err := db.QueryRow(context.Background(), "SELECT client, driver FROM trip_client WHERE id = $1", data[1]).Scan(&idClient, &idDriver); err != nil {
		return
	}
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "cancel", Data: model.EventTimeOutOrder{User: idClient, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "cancel", Data: model.EventTimeOutOrder{User: idDriver, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataTimeOutOrderInStateStart - Time-out created order
func PreparationDataTimeOutOrderInStateStart(db *pgx.Conn, data []string, act chan<- interface{}) {
	var idUser string
	db.QueryRow(context.Background(), "SELECT client FROM trip_client WHERE id = $1", data[1]).Scan(&idUser)
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "driver_out", Data: model.EventTimeOutOrder{User: idUser, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataTimeOutOrderInStateDriverReady - Time-out created order
func PreparationDataTimeOutOrderInStateDriverReady(db *pgx.Conn, data []string, act chan<- interface{}) {
	var idUser string
	db.QueryRow(context.Background(), "SELECT driver FROM trip_client WHERE id = $1", data[1]).Scan(&idUser)
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "client_out", Data: model.EventTimeOutOrder{User: idUser, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataTimeOutOrderInStateBeginTrip - Time-out created order
func PreparationDataTimeOutOrderInStateBeginTrip(db *pgx.Conn, data []string, act chan<- interface{}) {
	var idClient, idDriver string
	if err := db.QueryRow(context.Background(), "SELECT client, driver FROM trip_client WHERE id = $1", data[1]).Scan(&idClient, &idDriver); err != nil {
		return
	}
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "check", Data: model.EventTimeOutOrder{User: idClient, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
	dataJSON, _ = json.Marshal(model.WorkModelSend{Type: "orders", Object: "check", Data: model.EventTimeOutOrder{User: idDriver, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataTimeOutOrderInStateCreated - Time-out created order
func PreparationDataTimeOutOrderInStateCreated(db *pgx.Conn, data []string, act chan<- interface{}) {
	var idUser string
	db.QueryRow(context.Background(), "SELECT client FROM trip_client WHERE id = $1", data[1]).Scan(&idUser)
	dataJSON, _ := json.Marshal(model.WorkModelSend{Type: "orders", Object: "time_out", Data: model.EventTimeOutOrder{User: idUser, Order: data[1]}})
	createActionFromDatabase(dataJSON, act)
}

//PreparationDataOfferOrder data -> 0.- offer 1.- idUser 2.- duration 3.- lat 4.- lon 5.- creator order
func PreparationDataOfferOrder(db *pgx.Conn, data []string, act chan<- interface{}) {
	//Name, Phone, Img, Car, Partner
	var modelWorkModelDriverOrderDataOffer model.WorkModelDriverOrderDataOffer
	var modelDriverOffer model.DriverOrderDataOffer
	var idCar, cabinet string
	var img bool
	var err error
	modelDriverOffer.IDDriver = data[1]
	modelDriverOffer.IDUser = data[5]
	modelDriverOffer.Duration, err = strconv.Atoi(data[2])
	modelDriverOffer.Lat, err = strconv.ParseFloat(data[3], 64)
	modelDriverOffer.Lon, err = strconv.ParseFloat(data[4], 64)
	if err = db.QueryRow(context.Background(),
		"SELECT us.surname || ' ' || us.username AS name, us.phone, us.img, CASE WHEN (dr.car_arend = NULL OR dr.car_arend = '') THEN dr.car ELSE dr.car_arend END AS car FROM users AS us, drivers AS dr WHERE us.id = $1 AND dr.id = us.id",
		data[1]).Scan(&modelDriverOffer.Name, &modelDriverOffer.Phone, &img, &idCar); err != nil {
		fmt.Println("User data ", err.Error())
		return
	}
	if err = db.QueryRow(context.Background(),
		"SELECT mk.mark, cl.color, cl.code, car.num_reg, CASE WHEN ((car.owner_cabinet != NULL) OR (car.owner_cabinet != '')) THEN car.owner_cabinet ELSE 'no' END AS cab FROM cars AS car, color_auto AS cl, mark AS mk WHERE car.id = $1 AND cl.id = car.color AND mk.id = car.mark",
		idCar).Scan(&modelDriverOffer.Car.Mark, &modelDriverOffer.Car.Color.Title, &modelDriverOffer.Car.Color.Value, &modelDriverOffer.Car.Number, &cabinet); err != nil {
		fmt.Println("Car data - ", err.Error())
		return
	}
	if (len(cabinet) > 2) && (cabinet != "no") {
		if err = db.QueryRow(context.Background(), "SELECT id, title_ps FROM cabinet_ps WHERE id = $1",
			cabinet).Scan(&modelDriverOffer.Partner.ID, &modelDriverOffer.Partner.Name); err != nil {
			fmt.Println("cabinet data - ", err.Error())
			return
		}
	}
	if img {
		modelDriverOffer.Img = fmt.Sprintf("%v/media/usr?phone=%v&token=%v", URLFULLDRIVER, modelDriverOffer.Phone, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
	}
	modelWorkModelDriverOrderDataOffer.Type = "orders"
	modelWorkModelDriverOrderDataOffer.Object = "offer"
	modelWorkModelDriverOrderDataOffer.Data = modelDriverOffer
	dataJSON, _ := json.Marshal(modelWorkModelDriverOrderDataOffer)
	createActionFromDatabase(dataJSON, act)
}

//RecoveryOrderTimeOut data -> 0.- recovery 1.- idOrder 2.- tripStatus
func RecoveryOrderTimeOut(db *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	listOrder := wo.GetStepOrdersWithStateOnCancel()
	val, ok := listOrder[data[1]]
	if !ok || val.city == "" {
		return
	}
	fmt.Printf("Recovery - %v, %v\n", data[1], val.lastState)
	timingCity := wo.GetTimingCity()
	city := val.city
	fmt.Println("city - ", city)
	fmt.Println("time - ", timingCity[city].timeWaitingClient)
	if "created" == val.lastState && data[2] == val.lastState {
		wo.DelInAllSatate(data[1], true)
		wo.SetStepOrdersWithStateCreated(data[1], time.Now().Unix()+int64(timingCity[city].timeLiveOrderCreated), city)
	}
}

//RecoveryOrderDriverOut data -> 0.- recovery 1.- idOrder 2.- tripStatus
func RecoveryOrderDriverOut(db *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	listOrder := wo.GetStepOrdersWithStateOnCancel()
	val, ok := listOrder[data[1]]
	if !ok || val.city == "" {
		return
	}
	fmt.Printf("Recovery - %v, %v\n", data[1], val.lastState)
	timingCity := wo.GetTimingCity()
	city := val.city
	fmt.Println("city - ", city)
	fmt.Println("time - ", timingCity[city].timeWaitingClient)
	if "driver_ready" == val.lastState && data[2] == val.lastState {
		wo.DelInAllSatate(data[1], true)
		wo.SetStepOrdersWithStateDriverReady(data[1], time.Now().Unix()+int64(timingCity[city].timeWaitingClient), city)
	} else if "start" == val.lastState && data[2] == val.lastState {
		wo.DelInAllSatate(data[1], true)
		wo.SetStepOrdersWithStateStart(data[1], time.Now().Unix()+int64(timingCity[city].timeWaitingDriver), city)
	}
}

//RecoveryOrderClientOut data -> 0.- recovery 1.- idOrder 2.- tripStatus
func RecoveryOrderClientOut(db *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	listOrder := wo.GetStepOrdersWithStateOnCancel()
	val, ok := listOrder[data[1]]
	if !ok || val.city == "" {
		return
	}
	fmt.Printf("Recovery - %v, %v\n", data[1], val.lastState)
	timingCity := wo.GetTimingCity()
	city := val.city
	fmt.Println("city - ", city)
	fmt.Println("time - ", timingCity[city].timeWaitingClient)
	if "driver_ready" == val.lastState && data[2] == val.lastState {
		wo.DelInAllSatate(data[1], true)
		wo.SetStepOrdersWithStateDriverReady(data[1], time.Now().Unix()+int64(timingCity[city].timeWaitingClient), city)
	}
}

//RecoveryOrderPriceUP data -> 0.- recovery 1.- idOrder 2.- tripStatus
func RecoveryOrderPriceUP(db *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	listOrder := wo.GetStepOrdersWithStateOnCancel()
	val, ok := listOrder[data[1]]
	if !ok || val.city == "" {
		return
	}
	fmt.Printf("Recovery - %v, %v\n", data[1], val.lastState)
	timingCity := wo.GetTimingCity()
	city := val.city
	if "driver_ready" == val.lastState && data[2] == val.lastState {
		wo.DelInAllSatate(data[1], true)
		wo.SetStepOrdersWithStateDriverReady(data[1], time.Now().Unix()+int64(timingCity[city].timeWaitingClient), city)
	}
}

//RecoveryOrderCheck data -> 0.- recovery 1.- idOrder 2.- tripStatus
func RecoveryOrderCheck(db *pgx.Conn, wo *WorkersObject, data []string, act chan<- interface{}) {
	listOrder := wo.GetStepOrdersWithStateOnCancel()
	val, ok := listOrder[data[1]]
	if !ok {
		return
	}
	fmt.Printf("Recovery - %v, %v\n", data[1], val.lastState)
	timingCity := wo.GetTimingCity()
	if "begin_trip" == val.lastState && data[2] == val.lastState {
		wo.SetStepOrdersWithStateBeginTrip(data[1], time.Now().Unix()+int64(timingCity[val.city].timeLiveOrderTrip), val.city)
		wo.DelStepOrdersWithStateOnCancel(data[1])
	}
}

//PreparationDataActionPlan
func PreparationDataActionPlan(db *pgx.Conn, data []string, act chan<- interface{}) { //ADD

}

//PreparationDataActionPlanOut
func PreparationDataActionPlanOut(db *pgx.Conn, data []string) {
}
