package server

import (
	"fmt"

	"../../data/database"
	"../../utility"
	"../scar"
)

var errorCore = "Core module error#"

var core Core

//Core - is a storage structure for Core units
type Core struct {
	conf     *utility.Configuration
	proc     *HTTPProcess
	db       *database.Database
	localKey string
	sc       *scar.SCAR
	wc       *WebConn
	wd       *WebDriver
}

//InitCore - initialization core
func InitCore(path string) error {
	conf, err := utility.LoadFile(path) //Read config file
	if err != nil {
		return err
	}
	if !conf.IsValid() {
		return utility.ErrorCreate(errorCore, "InitCore.- In configuration file attend empty fields... ")
	}
	// scar, err := scar.SynchWithScar(conf.ScarAddrServer, conf.ScarCrt, conf.ScarKey, conf.ScarID)
	// if err != nil {
	// 	return utility.ErrorCreate(errorCore, ".InitCore.SynchWithScar.")
	// }
	core = Core{conf, InitServer(), database.DBInstance(conf.ConnectStrPg), conf.ScarID, &scar.SCAR{}, &WebConn{connect: make(map[string]WebSocketConnect)}, &WebDriver{connect: make(map[string]WebSocketConnect)}}
	return nil
}

//Start - starting core
func Start(err chan<- string, act chan interface{}) error {
	if errDb := core.db.InitDatabaseConnections(); errDb != nil {
		err <- errDb.Error()
		return errDb
	}
	core.Handlers(err)
	fmt.Println("Server key = ", core.localKey)
	go core.db.BusHandlersVerificationAction(err, act, core.localKey)
	go core.db.BusHandlersOrderAction(err, act)
	go core.db.BusHandlersLocalSystem(err, act, core.localKey)
	go core.db.StartWorkers()
	go core.ActionFromDatabase(act)
	go core.wc.CheckAllConnects()
	if errProc := core.proc.StartProcess(core.conf); errProc != nil {
		err <- errProc.Error()
		return errProc
	}
	return nil
}

//DestructCore - destruct core
// func DestructCore() error {
// 	return core.proc.server.Shutdown()
// }
