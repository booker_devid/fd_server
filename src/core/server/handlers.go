package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"../../data/database"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second
	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second
	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

//Handlers - router for server
func (root *Core) Handlers(errCh chan<- string) {
	//Check server
	root.proc.router.HandleFunc("/fd", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("GET")
	//Выгрузка активных городов. (Для регистрации пользователей)
	root.proc.router.HandleFunc("/fd/cities", func(w http.ResponseWriter, r *http.Request) { //READY
		data, err := database.ListCities(root.db)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(401)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("GET")
	//Выгрузка всех городов.
	root.proc.router.HandleFunc("/fd/all-cities", func(w http.ResponseWriter, r *http.Request) { //READY
		data, err := database.ListAllCities(root.db)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(401)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("GET")
	//Выгрузка марок автомобилей.
	root.proc.router.HandleFunc("/fd/car/marks", func(w http.ResponseWriter, r *http.Request) { //READY
		data, err := database.ListMarksAuto(root.db)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(401)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("GET")
	//Выгрузка цветов автомобиля.
	root.proc.router.HandleFunc("/fd/car/colors", func(w http.ResponseWriter, r *http.Request) { //READY
		data, err := database.ListСolorsAuto(root.db)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(401)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("GET")
	//Запрос адреса по координатам.
	root.proc.router.HandleFunc("/fd/get/addr/coordinte/{lat}/{lon}", func(w http.ResponseWriter, r *http.Request) {
		lat, _ := mux.Vars(r)["lat"]
		lon, _ := mux.Vars(r)["lon"]
		var err error
		// addr := model.AddrHouse{}
		placeModel, err := database.TakeInfoAddr(root.db, lat, lon)
		data, err := json.Marshal(placeModel)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(405)
		} else {
			w.Header().Set("Content-Type", "application/json")
			if _, err := w.Write(data); err != nil {
				fmt.Println(err.Error())
			}
		}
	}).Methods("GET")
	//Запрос списков подключенных сокетов
	root.proc.router.HandleFunc("/fd/list", func(w http.ResponseWriter, r *http.Request) {
		var data []byte
		var err error
		driver := r.URL.Query().Get("driver")
		if driver == "true" {
			data, err = root.wd.ListWocketsUP()
		} else {
			data, err = root.wc.ListWocketsUP()
		}
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(405)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("GET")
	//
	root.proc.router.HandleFunc("/fd/rating", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		rating := r.URL.Query().Get("rating")
		if len(token) == 64 {
			err := database.RatingOrder(root.db, token, rating)
			if err != nil {
				errCh <- err.Error()
				w.WriteHeader(405)
			} else {
				w.WriteHeader(200)
			}
		}
	}).Methods("PUT")
	//Запрос адресов по наименованию
	root.proc.router.HandleFunc("/fd/get/addr/{city}", func(w http.ResponseWriter, r *http.Request) {
		cityEn, _ := mux.Vars(r)["city"]
		addrSearch := r.URL.Query().Get("search")
		fmt.Println(cityEn, " ", addrSearch)
		check, city, err := database.CheckCityEn(root.db, cityEn)
		if check == true && err == nil && len(addrSearch) != 0 {
			var (
				err  error
				data []byte
			)
			addrModel, err := database.SearchAddr(cityEn, city, addrSearch)
			if addrModel.Addr == nil {
				w.WriteHeader(204)
			} else {
				data, err = json.Marshal(addrModel)
			}
			if err != nil {
				errCh <- err.Error()
				w.WriteHeader(405)
			} else {
				w.Header().Set("Content-Type", "application/json")
				w.Write(data)
			}
		} else {
			w.WriteHeader(405)
		}
	}).Methods("GET")
	//Запрос координат по наименованию
	root.proc.router.HandleFunc("/fd/get/coordinate/{city}", func(w http.ResponseWriter, r *http.Request) {
		city, _ := mux.Vars(r)["city"]
		check, _, err := database.CheckCityEn(root.db, city)
		if check == true && err == nil {
			var (
				err  error
				data []byte
			)
			addrModel, err := database.CheckAddr(city, r)
			if err == nil {
				data, err = json.Marshal(addrModel)
				if err != nil {
					errCh <- err.Error()
					w.WriteHeader(405)
				} else {
					w.Header().Set("Content-Type", "application/json")
					w.Write(data)
				}
			} else {
				errCh <- err.Error()
				w.WriteHeader(405)
			}
		} else {
			w.WriteHeader(405)
		}
	}).Methods("PUT")
	root.proc.router.HandleFunc("/fd/map-error", func(w http.ResponseWriter, r *http.Request) {
		err := database.MapError(root.db, r)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(401)
		} else {
			w.WriteHeader(200)
		}
	}).Methods("POST")
	//История поездок. (Пользователь)
	// root.proc.router.HandleFunc("/fd/order/recovery", func(w http.ResponseWriter, r *http.Request) {
	// 	token := r.URL.Query().Get("token")
	// 	order := r.URL.Query().Get("order")
	// 	if len(token) > 0 && len(order) > 0 {
	// 		err := database.RecoveryOrderInBusAction(root.db, order)
	// 		if err != nil {
	// 			w.WriteHeader(200)
	// 		} else {
	// 			w.WriteHeader(401)
	// 		}
	// 	} else {
	// 		w.WriteHeader(405)
	// 	}
	// }).Methods("PUT")
	//История поездок. (Водитель)
	root.proc.router.HandleFunc("/fd/trip/driver/story", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Отмена заказа. (При исполнении)
	root.proc.router.HandleFunc("/fd/trip/cancel", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	// Выбор водителя.
	root.proc.router.HandleFunc("/fd/trip/driver", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Отменить заказ.
	root.proc.router.HandleFunc("/fd/trip/drop", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Выбор заказа - межгород.
	root.proc.router.HandleFunc("/fd/trip/outcity", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Выбор заказа - город.
	root.proc.router.HandleFunc("/fd/trip/incity", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Запланировать поездку.
	root.proc.router.HandleFunc("/fd/trip/driver/outcity", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Водитель подъехал.
	root.proc.router.HandleFunc("/fd/trip/driver/ready", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Клиент выходит.
	root.proc.router.HandleFunc("/fd/trip/client/ready", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Заказ завершен.
	root.proc.router.HandleFunc("/fd/trip/ready", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Отзыв о клиенте.
	root.proc.router.HandleFunc("/fd/trip/rating/client", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Отзыв о водителе.
	root.proc.router.HandleFunc("/fd/trip/rating/driver", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Покупка сессии.
	root.proc.router.HandleFunc("/fd/driver/session", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Отвязка автомобиля.
	root.proc.router.HandleFunc("/fd/driver/untie/car", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Привязка автомобиля.
	root.proc.router.HandleFunc("/fd/driver/binding/car", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Просмотр предложений на аренду.
	root.proc.router.HandleFunc("/fd/driver/check/reand", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Взять автомобить в аренду.
	root.proc.router.HandleFunc("/fd/driver/take/auto", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("PUT")
	//Получение информации о заказе для клиента.
	root.proc.router.HandleFunc("/fd/order", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		driver := r.URL.Query().Get("driver")
		fmt.Println("socket: connect with token - ", token)
		if len(token) != 0 {
			_, idUser, err := database.CheckingUserToken(root.db, token)
			if err == nil {
				fmt.Println("socket: connect - " + idUser)
				conn, err := upgrader.Upgrade(w, r, nil) // error ignored for sake of simplicity
				root.wc.CheckWebConnects(conn)
				// conn, err := websocket.
				if err != nil {
					fmt.Println("Error: synch client read message") //debug index
					conn.Close()
				} else {
					fmt.Println("socket: new connect " + conn.RemoteAddr().String())
					modelWS := WebSocketConnect{}
					modelWS.Addr = conn.RemoteAddr().String()
					modelWS.WSConn = conn
					if driver == "true" {
						idCity, _ := database.TakeCityName(root.db, idUser)
						modelWS.City = idCity
						root.wd.SetWebConnectsDriver(idUser, modelWS)
						data, err := database.LoadOrders(root.db, idCity)
						if err == nil {
							conn.WriteMessage(1, data)
						}
					}
					root.wc.SetWebConnects(idUser, modelWS)
					go modelWS.WSReaderClient(idUser, root.db, root.wc, root.wd, root.localKey)
					// root.wc.CheckWebConnects(conn)
				}
				// root.wc.CheckWebConnects(conn)
			} else {
				w.WriteHeader(401)
				return
			}
		} else {
			w.WriteHeader(401)
			return
		}
	})
	/************F*O*R***S*E*R*V*E*R***A*N*D***P*A*R*T*N*E*R************/
	root.proc.router.HandleFunc("/fd/s-ad/users", func(w http.ResponseWriter, r *http.Request) {
		data := database.TestModelClientOrder()
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}).Methods("GET")
	root.proc.router.HandleFunc("/fd/s-ad/drivers", func(w http.ResponseWriter, r *http.Request) {
		data := database.TestModelClientOrder()
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}).Methods("GET")
	root.proc.router.HandleFunc("/fd/s-ad/drivers/verification", func(w http.ResponseWriter, r *http.Request) {
		data := database.TestModelClientOrder()
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}).Methods("GET")
	root.proc.router.HandleFunc("/fd/s-ad/cars", func(w http.ResponseWriter, r *http.Request) {
		data := database.TestModelClientOrder()
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}).Methods("GET")
	root.proc.router.HandleFunc("/fd/s-ad/cars/verification", func(w http.ResponseWriter, r *http.Request) {
		data := database.TestModelClientOrder()
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}).Methods("GET")
	//TestVerification car
	root.proc.router.HandleFunc("/fd/s-ad/test/car-verification", func(w http.ResponseWriter, r *http.Request) {
		idCar := r.URL.Query().Get("id")
		if len(idCar) > 2 && len(idCar) < 16 {
			fmt.Println(idCar)
			err := database.CarVerification(root.db, idCar)
			if err != nil {
				w.WriteHeader(405)
			} else {
				w.WriteHeader(200)
			}
		} else {
			w.WriteHeader(405)
		}
	}).Methods("GET")
	//TestVerification driver
	root.proc.router.HandleFunc("/fd/s-ad/test/driver-verification", func(w http.ResponseWriter, r *http.Request) { //
		idDriver := r.URL.Query().Get("id")
		if len(idDriver) > 3 {
			err := database.DriverVerification(root.db, idDriver)
			if err != nil {
				w.WriteHeader(405)
			} else {
				w.WriteHeader(200)
			}
		} else {
			w.WriteHeader(405)
		}
	}).Methods("GET")
	root.proc.router.HandleFunc("/fd/s-ad/drivers/verification", func(w http.ResponseWriter, r *http.Request) {
		data := database.TestModelClientOrder()
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}).Methods("POST")
	root.proc.router.HandleFunc("/fd/s-ad/cars/verification", func(w http.ResponseWriter, r *http.Request) {
		data := database.TestModelClientOrder()
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}).Methods("POST")
}

func writeAnswerSystemHandlersWithData(check int, data []byte, err error, w http.ResponseWriter, errCh chan<- string) {
	if check == 1 {
		w.WriteHeader(401)
	} else if check == 2 {
		w.WriteHeader(201)
	} else if check == 3 {
		w.WriteHeader(523)
	} else if err != nil {
		errCh <- err.Error()
		w.WriteHeader(405)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}
}

func writeAnswerSystemHandlersWithOutData(check int, err error, w http.ResponseWriter, errCh chan<- string) {
	if check == 1 {
		w.WriteHeader(401)
	} else if check == 2 {
		w.WriteHeader(201)
	} else if check == 3 {
		w.WriteHeader(523)
	} else if err != nil {
		errCh <- err.Error()
		w.WriteHeader(405)
	} else {
		w.WriteHeader(405)
	}
}
