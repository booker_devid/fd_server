package utility

import (
	"encoding/json"
	"io/ioutil"
)

var errorLoad = "Configuration loader module error#"

//LoadFile function is reading config file
func LoadFile(path string) (*Configuration, error) {
	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, ErrorHandler(errorLoad, err)
	}
	return unmarshalToObjConf(fileBytes)
}

//UnmarshalToObjConf function is converting strings in string array
func unmarshalToObjConf(confBytes []byte) (*Configuration, error) {
	var config Configuration
	if err := json.Unmarshal(confBytes, &config); err != nil {
		return nil, ErrorHandler(errorLoad, err)
	}
	return &config, nil
}
